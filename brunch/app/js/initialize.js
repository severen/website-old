import 'what-input';
import 'foundation-sites';

import hljs from 'highlight.js';

$(document).foundation();
hljs.initHighlightingOnLoad();
