module.exports = {
    files: {
        javascripts: {
            joinTo: {
                'js/vendor.js': /^(?!app)/,
                'js/app.js': /^app/,
            },
        },
        stylesheets: { joinTo: 'css/styles.css' },
    },

    paths: {
        public: '../assets',
    },

    plugins: {
        babel: { presets: ['es2015'] },
        sass: {
            precision: 10,
            options: {
                includePaths: [
                    'node_modules/foundation-sites/scss',
                    'node_modules/font-awesome/scss',
                    'node_modules/highlight.js/styles',
                ],
            },
        },
        postcss: {
            processors: [
                require('autoprefixer')(
                    ['last 2 versions', 'ie >= 9', 'and_chr >= 2.3']
                ),
            ],
        },
    },

    npm: {
        globals: {
            $: 'jquery',
            jQuery: 'jquery',
        },
    },
};
