# Shrike's Website

[![Travis CI][travis-ci-badge]][travis-ci]

This is the source code for my [personal blog](https://shrike.me/).

[travis-ci]: https://travis-ci.org/SShrike/website
[travis-ci-badge]: https://img.shields.io/travis/SShrike/website.svg
