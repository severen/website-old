title: Om meg
---
body:

Hei, jeg er Severen. I typically go by the pseudonyms 'Shrike' *or* 'SShrike'
online, with the latter only being used when 'Shrike' is already claimed; feel
free to call me any of the aforementioned names, I don't have a preference.

I have a passion for open-source software, the art of programming, and the
general pursuit of knowledge.

>People think that computer science is the art of geniuses but the actual
>reality is the opposite, just many people doing things that build on each
>other, like a wall of mini stones. — Donald Knuth

My main — programming related — interests are *systems programming*,
*communications*, *developer tooling*, and the general field of *computer
science* — especially [programming language theory][PLT] (PLT) and its related
branches. Some other — non programming related — interests of mine are
*linguistics*, *history*, *science* (especially physics and astronomy), and
*competitive gaming*.

The programming languages I currently find myself working with daily are
[Python] and [Rust]. Some other languages I very much enjoy but haven't found a
day-to-day use for yet are [Haskell], [Elixir], [Erlang], and [Elm].

Check out my [GitHub page](https://github.com/SShrike) if you want to check out
what I've been up to recently.

## Kryptografi

Use the following profiles to identify my identity on social media, etc.:

* [Keybase Profile](https://keybase.io/sshrike)
* [OneName Profile](https://onename.com/shrike)

My GPG public key is available [here](https://keybase.io/SShrike/key.asc).

<!-- Links -->
[PLT]: https://en.wikipedia.org/wiki/Programming_language_theory
[Python]: https://python.org/
[Rust]: https://www.rust-lang.org/
[Haskell]: https://www.haskell.org/
[Elixir]: http://elixir-lang.org/
[Erlang]: https://www.erlang.org/
[Elm]: http://elm-lang.org/
